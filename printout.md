# Must do for your phone

A culture of caring about security and taking basic steps to protect each other reduces the potential for harm.

## Lock phone with unique passcode

Numeric-only, an easier keypad, is OK.  Make it 7+ characters no one can see how long it is.

Why:

  * Phone is critical; must be a passcode you have memorized and which you do not use for anything else.
  * Most all the people you organize with are in your phone
  * Biometrics can be faked.
  * Police can (legally, physically) compel you to unlock with face, fingerprint

How:

  * Android
  * iPhone

## Disable notifications on the lock screen

Why:

  * Notifications can show a lot of information and metadata (who is contacting you when) even with the phone locked.

How:

  * Android
  * iPhone

## Signal

### Set up

  * Use a 6+ digit PIN and memorize it.
  * Enable registration lock (only after you have definitely memorized your Signal PIN, this prevents setting up a new Signal without the PIN.)

### Usage

  * Know who is in the loop.
  * If it's so large you don't immediately know who is in the loop, assume it is effectively public.
  * Leave and delete loops when done (two steps; first leave, then delete).


## Encrypt phone

  * Android
  * iPhone

## Safe practices

  * Do not install an app by clicking a link; go to the app store and search for the app you want to install.
  * Do not store new contacts to Google nor to SIM card, but (if adding a contact is needed at all) to phone.

## Other settings

  * Enable automatic software updates
  * Enable MAC Address Randomization
  * On iPhone, disable AirDrop.
  * Disable location tracking or remove apps that request location tracking permissions.

## Lock PIN

  * Android - https://www.howtogeek.com/259360/how-to-set-up-sim-card-lock-for-a-more-secure-android-phone/
  * iPhone

## Set up port validation with your carrier

Add a PIN or passcode to your wireless account with AT&T, Verizon, T-Mobile or other carrier.  This is something you will enter into their website or tell a company representative when getting a new phone or switching carriers, not something you enter into your phone.

https://www.thebalanceeveryday.com/prevent-your-mobile-number-from-being-ported-4160360

This should also make it less likely that your provider will let someone impersonate you to get your number on their phone, https://www.cnet.com/how-to/sim-swap-fraud-how-to-prevent-your-phone-number-from-being-stolen/

## When you do not want your location known

Ideally, you would leave your phone at home, or turn it off and put it in a Faraday bag.  When that is not practical:

  * Put into airplane mode
  * Enable Wi-Fi if data needed and trusted-ish network is available.
  * Disable bluetooth

# General

  * Never rely on technology.
  * Don't put anything in writing or a recording you wouldn't want in public or court.

# More information

https://ssd.eff.org/
